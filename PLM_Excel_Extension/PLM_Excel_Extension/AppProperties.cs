﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLM_Excel_Extension
{
    public static class AppProperties
    {
        public static string FilePath { get; set; }
        public static string Address { get; set; }
    }
}
