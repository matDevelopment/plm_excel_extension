﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace PLM_Excel_Extension.Helpers
{
    public static class XmlHelper
    {
        public static List<ExcelProperty> ReadPropertyDefinitonsFromXml()
        {
            XDocument doc = XDocument.Load("C:\\excel\\config.xml");
            var propertyDefXml = doc.Descendants("PropertyDefinitions").Descendants("PropertyDefinition").ToList();
            var excelProperties = new List<ExcelProperty>();
            foreach (var property in propertyDefXml)
            {
                excelProperties.Add(new ExcelProperty {
                    Name = property.Attribute("Value").Value
                });
            }
            return excelProperties;
        }
        public static void CreateXmlConfig(List<ExcelProperty> propertyDefinitions)
        {
            var file = FileHelper.CreateFile("C:\\excel", "configExport.xml");
            XmlDocument doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);
            XmlNode propertyDefinitionsNode = doc.CreateElement("PropertyDefinitions");
            doc.AppendChild(propertyDefinitionsNode);
            foreach (var prop in propertyDefinitions)
            {
                XmlNode propertyDefinition = doc.CreateElement("PropertyDefinition");
                XmlAttribute valueAttr = doc.CreateAttribute("Value");
                valueAttr.Value = prop.Name;
                propertyDefinition.Attributes.Append(valueAttr);
                XmlAttribute xAttr = doc.CreateAttribute("X");
                xAttr.Value = prop.AddressX;
                propertyDefinition.Attributes.Append(xAttr);
                XmlAttribute yAttr = doc.CreateAttribute("Y");
                yAttr.Value = prop.AddressY;
                propertyDefinition.Attributes.Append(yAttr);
                propertyDefinitionsNode.AppendChild(propertyDefinition);
            }
            doc.Save(file.FullName);
        }
    }
}
