﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLM_Excel_Extension.Helpers
{
    public static class FileHelper
    {
        public static FileInfo CreateFile(string path, string fileName)
        {
            try
            {
                FileInfo file = new FileInfo(Path.Combine(path, fileName));
                if (file.Exists)
                {
                    file.Delete();
                    file = new FileInfo(Path.Combine(path, fileName));
                }
                else
                {
                    file = new FileInfo(Path.Combine(path, fileName));
                }
                return file;
            }
            catch
            {
                return null;
            }

        }
    }
}
