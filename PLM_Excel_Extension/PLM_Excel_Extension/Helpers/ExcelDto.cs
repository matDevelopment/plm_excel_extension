﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PLM_Excel_Extension.Helpers
{
    public class ExcelDto
    {
        public ByteArrayContent Data { get; set; }
    }
}
