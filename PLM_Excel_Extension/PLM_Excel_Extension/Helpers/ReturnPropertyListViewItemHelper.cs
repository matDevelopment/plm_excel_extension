﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLM_Excel_Extension.Helpers
{
    public class ReturnPropertyListViewItemHelper
    {
        public ExcelProperty Property { get; set; }
        public ListViewItem ListViewItem { get; set; }
    }
}
