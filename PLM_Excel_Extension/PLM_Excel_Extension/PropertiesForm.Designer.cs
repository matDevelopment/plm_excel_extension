﻿namespace PLM_Excel_Extension
{
    partial class PropertiesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SendButton = new System.Windows.Forms.Button();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.PropertiesListView = new System.Windows.Forms.ListView();
            this.NameColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PickedPropertiesListView = new System.Windows.Forms.ListView();
            this.PickedNameColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PickedXColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PickedYColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.OpenFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.fileName = new System.Windows.Forms.Label();
            this.FileDialogPrompt = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // SendButton
            // 
            this.SendButton.Location = new System.Drawing.Point(14, 594);
            this.SendButton.Margin = new System.Windows.Forms.Padding(5);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(126, 42);
            this.SendButton.TabIndex = 3;
            this.SendButton.Text = "Wyślij";
            this.SendButton.UseVisualStyleBackColor = true;
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // PropertiesListView
            // 
            this.PropertiesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NameColumn});
            this.PropertiesListView.FullRowSelect = true;
            this.PropertiesListView.GridLines = true;
            this.PropertiesListView.Location = new System.Drawing.Point(14, 75);
            this.PropertiesListView.Margin = new System.Windows.Forms.Padding(5);
            this.PropertiesListView.MultiSelect = false;
            this.PropertiesListView.Name = "PropertiesListView";
            this.PropertiesListView.Size = new System.Drawing.Size(287, 509);
            this.PropertiesListView.TabIndex = 4;
            this.PropertiesListView.UseCompatibleStateImageBehavior = false;
            this.PropertiesListView.View = System.Windows.Forms.View.Details;
            this.PropertiesListView.SelectedIndexChanged += new System.EventHandler(this.PropertiesListView_SelectedIndexChanged);
            // 
            // NameColumn
            // 
            this.NameColumn.Text = "Nazwa";
            this.NameColumn.Width = 280;
            // 
            // PickedPropertiesListView
            // 
            this.PickedPropertiesListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.PickedNameColumn,
            this.PickedXColumn,
            this.PickedYColumn});
            this.PickedPropertiesListView.FullRowSelect = true;
            this.PickedPropertiesListView.GridLines = true;
            this.PickedPropertiesListView.Location = new System.Drawing.Point(318, 75);
            this.PickedPropertiesListView.Margin = new System.Windows.Forms.Padding(5);
            this.PickedPropertiesListView.MultiSelect = false;
            this.PickedPropertiesListView.Name = "PickedPropertiesListView";
            this.PickedPropertiesListView.Size = new System.Drawing.Size(347, 561);
            this.PickedPropertiesListView.TabIndex = 5;
            this.PickedPropertiesListView.UseCompatibleStateImageBehavior = false;
            this.PickedPropertiesListView.View = System.Windows.Forms.View.Details;
            this.PickedPropertiesListView.SelectedIndexChanged += new System.EventHandler(this.PickedPropertiesListView_SelectedIndexChanged);
            // 
            // PickedNameColumn
            // 
            this.PickedNameColumn.Text = "Nazwa";
            this.PickedNameColumn.Width = 218;
            // 
            // PickedXColumn
            // 
            this.PickedXColumn.Text = "X";
            this.PickedXColumn.Width = 61;
            // 
            // PickedYColumn
            // 
            this.PickedYColumn.Text = "Y";
            this.PickedYColumn.Width = 63;
            // 
            // OpenFile
            // 
            this.OpenFile.Location = new System.Drawing.Point(148, 594);
            this.OpenFile.Name = "OpenFile";
            this.OpenFile.Size = new System.Drawing.Size(153, 42);
            this.OpenFile.TabIndex = 6;
            this.OpenFile.Text = "Otwórz plik";
            this.OpenFile.UseVisualStyleBackColor = true;
            this.OpenFile.Click += new System.EventHandler(this.OpenFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(141, 23);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nazwa Pliku :";
            // 
            // fileName
            // 
            this.fileName.AutoSize = true;
            this.fileName.Location = new System.Drawing.Point(159, 13);
            this.fileName.Name = "fileName";
            this.fileName.Size = new System.Drawing.Size(0, 23);
            this.fileName.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 23);
            this.label2.TabIndex = 9;
            this.label2.Text = "Status :";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(101, 47);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(67, 23);
            this.StatusLabel.TabIndex = 10;
            this.StatusLabel.Text = "Edycja";
            // 
            // PropertiesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(679, 650);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fileName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OpenFile);
            this.Controls.Add(this.PickedPropertiesListView);
            this.Controls.Add(this.PropertiesListView);
            this.Controls.Add(this.SendButton);
            this.Font = new System.Drawing.Font("RomanD", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "PropertiesForm";
            this.Text = "PLM";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button SendButton;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.ListView PropertiesListView;
        private System.Windows.Forms.ColumnHeader NameColumn;
        private System.Windows.Forms.ListView PickedPropertiesListView;
        private System.Windows.Forms.ColumnHeader PickedNameColumn;
        private System.Windows.Forms.ColumnHeader PickedXColumn;
        private System.Windows.Forms.ColumnHeader PickedYColumn;
        private System.Windows.Forms.Button OpenFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label fileName;
        private System.Windows.Forms.OpenFileDialog FileDialogPrompt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label StatusLabel;
    }
}