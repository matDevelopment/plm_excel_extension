﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLM_Excel_Extension
{
    class ThreadForm
    {
        Thread th;
        private Form _form;
        private Form _thisForm;

        public ThreadForm(Form form, Form thisForm)
        {
            _form = form;
            _thisForm = thisForm;
        }
        public void setForms(Form form, Form thisForm)
        {
            _form = form;
            _thisForm = thisForm;
        }
        public void RunForm()
        {
            th = new Thread(() => { Application.Run(_form); });
            th.SetApartmentState(ApartmentState.STA);
            th.Start();
            _thisForm.Close();
        }
    }
}
