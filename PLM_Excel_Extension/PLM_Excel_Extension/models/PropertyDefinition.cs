﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLM_Excel_Extension.models
{
   public class PropertyDefinition
    {
        public int Id { get; set; }
        public string SystemName { get; set; }
        public string DisplayName { get; set; }
        
    }
}
