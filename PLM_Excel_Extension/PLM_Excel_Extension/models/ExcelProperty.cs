﻿namespace PLM_Excel_Extension
{
    public class ExcelProperty
    {
        public string Name { get; set; }
        public string AddressX { get; set; }
        public string AddressY { get; set; }
    }
}
