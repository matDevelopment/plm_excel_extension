﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLM_Excel_Extension.models
{
    public class JsonModel
    {
        public List<ExcelProperty> ExcelProperties { get; set; }
        public string FileName { get; set; }
    }
}
