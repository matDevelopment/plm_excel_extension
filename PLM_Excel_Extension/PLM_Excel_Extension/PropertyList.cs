using Newtonsoft.Json;
using PLM_Excel_Extension.Helpers;
using PLM_Excel_Extension.models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PLM_Excel_Extension
{
    public static class PropertyList
    {
        public static List<ExcelProperty> Create()
        {
            CancellationToken cancelToken = default(CancellationToken);
            var client = new HttpClient();
            client.BaseAddress = new Uri(AppProperties.Address, UriKind.Absolute);
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json; charset=utf-8");
     
            var response = client.GetAsync(AppProperties.Address + "/api/Bom/PropertyDefinitions", cancelToken).Result;
            var json = response.Content.ReadAsStringAsync().Result;
            var result = JsonConvert.DeserializeObject<List<PropertyDefinition>>(json); // z Geta wcale nie musi przyjsc jeden wynik

            var properties = new List<ExcelProperty>();

            foreach (var prop in result)
            {
                var tmpProp = new ExcelProperty
                {
                    Name = prop.DisplayName
                };
                properties.Add(tmpProp);
            }

            return properties;
        }

    }
}
