﻿using Newtonsoft.Json;
using PLM_Excel_Extension.Helpers;
using PLM_Excel_Extension.models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace PLM_Excel_Extension
{
    public partial class PropertiesForm : Form
    {
        List<ExcelProperty> _properties = new List<ExcelProperty>();
        List<ExcelProperty> _pickedProperties = new List<ExcelProperty>();
        ExcelProperty _editedProperty = null;
        ExcelProperty _lastEditedProperty = new ExcelProperty();
        Excel.AppEvents_SheetSelectionChangeEventHandler _action = (obj, range) => { };
        Excel.Application _excelApp;
        ListView.SelectedListViewItemCollection _album;
        
        public PropertiesForm()
        {
            InitializeComponent();
            //AppProperties.Address = File.ReadAllText(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\config.txt");
            //_properties = PropertyList.Create();
            _properties = XmlHelper.ReadPropertyDefinitonsFromXml();
            EditListView.Establish(_properties, PropertiesListView);

            // functions
            PickedPropertiesListView.KeyDown += PickedPropertiesListView_OnKeyDown;
            this.FormClosed += new FormClosedEventHandler(PropertiesForm_Closed);
            this.TopMost = true;
            SendButton.Enabled = true;
        }

        private void PropertiesForm_Closed(object sender, EventArgs e)
        {
            try
            {
                EditExcel.Kill(_excelApp);
            }
            catch (Exception) { }
            
        }

        private void PropertiesListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection album = PropertiesListView.SelectedItems;

            if (album.Count > 0)
            {
                var values = EditListView.Remove(_properties, album[0].Index, PropertiesListView);
                EditListView.Add(_pickedProperties, values.Property, values.ListViewItem, PickedPropertiesListView);
            }

            SendButton.Enabled = !IsNull(_pickedProperties);
        }

        private void PickedPropertiesListView_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                try { 
                    var values = EditListView.Remove(_pickedProperties, _album[0].Index, PickedPropertiesListView);
                    EditListView.Add(_properties, values.Property, values.ListViewItem, PropertiesListView);
                    EditExcel.RemoveOnSelectionChange(_excelApp, _action);
                    SendButton.Enabled = !IsNull(_pickedProperties);

                }
                catch (Exception)
                {
                    SendButton.Enabled = !IsNull(_pickedProperties);
                }
            }
        }

        private static bool IsNull(List<ExcelProperty> excelProperties)
        {
            if(excelProperties.Count == 0)
            {
                return true;
            }

            var isNull = false ;
            foreach(var excelProperty in excelProperties)
            {
                if(excelProperty.AddressX == null || excelProperty.Name == null || excelProperty.AddressY == null)
                {
                    
                    isNull = true;
                }
            }
            return isNull;
        }

        private void PickedPropertiesListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            _album = PickedPropertiesListView.SelectedItems;
             if (_album.Count > 0)
             {
                EditExcel.BringToFront(_excelApp);
                _editedProperty = _pickedProperties[_album[0].Index];
                 EditExcel.RemoveOnSelectionChange(_excelApp, _action);
                _action = EditExcel.AddOnSelectionChange(_excelApp, (obj, range) => {
                     String value = range.Address;
                     Char delimiter = '$';
                     String[] substrings = value.Split(delimiter);
                     PickedPropertiesListView.Invoke(new MethodInvoker(delegate {
                         try
                         {
                             _pickedProperties[_album[0].Index].AddressX = range.Cells.Column.ToString();
                             _pickedProperties[_album[0].Index].AddressY = range.Cells.Row.ToString();
                             EditListView.UpdatePicked(_editedProperty, PickedPropertiesListView.Items[_album[0].Index], substrings[1], substrings[2]);
                             SendButton.Enabled = !IsNull(_pickedProperties);
                         }
                         catch (Exception)
                         {
                             SendButton.Enabled = !IsNull(_pickedProperties);
                         }
                     }));
                 });
             }
        }

        private void OpenFile_Click(object sender, EventArgs e)
        {
            FileDialogPrompt.ShowDialog();
            string path = FileDialogPrompt.FileName;
            AppProperties.FilePath = path;
            fileName.Text = path;
            _excelApp = EditExcel.OpenConnection(AppProperties.FilePath);
            StatusLabel.Text = "Edycja";
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            var jsonProperties = JsonConvert.SerializeObject(new JsonModel { ExcelProperties = _pickedProperties, FileName = Path.GetFileNameWithoutExtension(AppProperties.FilePath) });
            EditExcel.Save(_excelApp, AppProperties.FilePath);
            XmlHelper.CreateXmlConfig(_pickedProperties);
         /*   using (var client = new WebClient())
            {
                Uri urlXml = new Uri(AppProperties.Address + "/api/upload/xml");
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                client.Encoding = Encoding.UTF8;
                string response = client.UploadString(urlXml, jsonProperties);
            }
            using (var client = new WebClient())
            {
                Uri urlExcel = new Uri(AppProperties.Address + "/api/upload/excel");
                client.Headers.Add("name", "file");
                client.UploadFileAsync(urlExcel, AppProperties.FilePath);
                StatusLabel.Text = jsonProperties.ToString();
            }*/
            StatusLabel.Text = "Pliki Dodane";
        }
    }
}
