﻿
using PLM_Excel_Extension.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace PLM_Excel_Extension
{
    public static class EditListView
    {
        public static void Establish(List<ExcelProperty> properties, ListView listView)
        {
            
            foreach (var property in properties.Select((value, i) => new { i, value }))
            {
                ListViewItem listViewItem = new ListViewItem(property.value.Name);
                listView.Items.Add(listViewItem);
            }
        }

        public static void UpdatePicked(ExcelProperty property, ListViewItem listViewItem, string x, string y)
        {
            var index = listViewItem.SubItems.Count;
            for (var i = 0; i < index; i++)
            {
                listViewItem.SubItems.RemoveAt(index - i - 1);
            }
            listViewItem.Text = property.Name;
            listViewItem.SubItems.Add(x);
            listViewItem.SubItems.Add(y);
        }

        public static ReturnPropertyListViewItemHelper Remove(List<ExcelProperty> properties, int index, ListView listView)
        {
            try
            {
                var property = properties[index];
                property.AddressX = null;
                property.AddressY = null;
                var listViewItem = listView.Items[index];
                properties.Remove(properties[index]);
                listView.Items[index].Remove();
                return new ReturnPropertyListViewItemHelper { Property = property, ListViewItem = listViewItem };
            }
            catch (System.Exception)
            {
                return null;
            }
            
        }

        public static void Add(List<ExcelProperty> properties, ExcelProperty property, ListViewItem listViewItem, ListView listView)
        {
            properties.Add(property);
            var listViewI = new ListViewItem(listViewItem.Text);
            listView.Items.Add(listViewI);
        }
    }
}
