﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace PLM_Excel_Extension
{
    public static class EditExcel
    {
        public static Excel.Application OpenConnection(string filePath)
        {
            try
            {
                var xlApp = new Excel.Application();
                xlApp.Visible = true;
                Excel.Workbooks books = xlApp.Workbooks;
                Excel.Workbook sheet = books.Open(filePath);
                return xlApp;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public static Excel.AppEvents_SheetSelectionChangeEventHandler AddOnSelectionChange(Excel.Application excelInstance, Action<object, Excel.Range> action)
        {
            try
            {
                var function = new Excel.AppEvents_SheetSelectionChangeEventHandler(action);
                excelInstance.SheetSelectionChange += function;
                return function;
            }
            catch (Exception)
            {
                return null;
            }
            
        }

        public static void RemoveOnSelectionChange(Excel.Application excelInstance, Excel.AppEvents_SheetSelectionChangeEventHandler selectionChangedEvent)
        {
            try
            {
                excelInstance.SheetSelectionChange -= selectionChangedEvent;
            }
            catch (Exception)
            {

            }
           
        }

        public static string TrimResponse (string response)
        {
            return response;
        }

        public static void Save(Excel.Application excelInstance, string excelfilePath)
        {
            
            var fileName = Path.GetFileNameWithoutExtension(excelfilePath);
            var filePath = Path.GetDirectoryName(excelfilePath);

            Excel.Workbook wbook = (Excel.Workbook)excelInstance.ActiveWorkbook;
            if (wbook != null)
            {
                wbook.SaveAs(filePath + fileName + "copy.xlsx", Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing,
                    false, false, Excel.XlSaveAsAccessMode.xlNoChange,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                wbook.Save();
                Kill(excelInstance);
                if (File.Exists(filePath + fileName + "copy.xlsx"))
                {
                    File.Delete(excelfilePath);
                    FileStream stream = null;
                    FileInfo fileInfo = new FileInfo(filePath + fileName + "copy.xlsx");
                    while (IsFileLocked(fileInfo))
                    {
                    }
                    File.Move(filePath + fileName + "copy.xlsx", filePath + fileName + ".xlsx");
                }
            } else
            {
                Kill(excelInstance);
            }
           
        }

        private static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        [DllImport("User32.dll")]
        public static extern int GetWindowThreadProcessId(IntPtr hWnd, out int ProcessId);
        public static void Kill(Excel.Application theApp)
        {
            int id = 0;
            IntPtr intptr = new IntPtr(theApp.Hwnd);
            System.Diagnostics.Process p = null;
            try
            {
                GetWindowThreadProcessId(intptr, out id);
                p = System.Diagnostics.Process.GetProcessById(id);
                if (p != null)
                {
                    p.Kill();
                    p.Dispose();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("KillExcel:" + ex.Message);
            }
            Marshal.FinalReleaseComObject(theApp);
        }

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        public static void BringToFront(Excel.Application theApp)
        {
            try
            {
                IntPtr intptr = new IntPtr(theApp.Hwnd);
                SetForegroundWindow(intptr);
            }
            catch (Exception)
            {
            }
        }
    }
}
